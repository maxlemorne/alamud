 
# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .event import Event2

class MoveEvent(Event2):
    NAME = "move"

    def perform(self):
        if not self.object.has_prop("movable"):
            self.fail()
            return self.inform("move.failed")
        self.inform("move")
